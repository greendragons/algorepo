#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define LIM 1000

typedef struct Node {
	int id;
	struct Node *next;
} node;

node *graph[LIM];
char visited[LIM];
char dist[LIM];
char dtime[LIM];
char ftime[LIM];

int q[LIM];

int t = 0;

void dfs(int s) {
	int u,v,i;
	visited[s] = 1;	
	dtime[s] = t++;
	node *temp;
	temp = graph[s];
	while(temp) {
		if(!visited[temp->id]) {
			dfs(temp->id);
		}	
		temp = temp->next;	
	}
	ftime[s] = t++;
}

void bfs(int s) {
	int front, rear,u;	
	node *temp;
	front = rear = 0;
	dist[s] = 0;
	visited[s] = 1;
	q[rear++] = s;
	u = s;
	while(rear != front) {
		u = q[front++]; 
		visited[u] = 1;
		temp = graph[u];
		while(temp) {
			if(!visited[temp->id]){
				dist[temp->id] = dist[u] + 1;
				q[rear++] = temp->id;
			}
			temp = temp->next;
		}
	}
}

int main() {

	int n,i,j,v,u,m;
	node *temp;
	scanf("%d%d",&n,&m);
	memset(graph,0,sizeof(graph));
	memset(dtime,0,sizeof(dtime));
	memset(ftime,0,sizeof(ftime));

	while(m--) {
		scanf("%d%d",&u,&v);
		temp = (node *)malloc(sizeof(node));
		temp->id = v;
		temp->next = graph[u]; 
		graph[u] = temp; 

		temp = (node *)malloc(sizeof(node));
		temp->id = u;
		temp->next = graph[v]; 
		graph[v] = temp; 

	}	
	
	for(i=0; i<n; i++) {
		temp = graph[i];
		while(temp) {
			printf("%d ",temp->id);
			temp = temp->next;
		}
		printf("\n");
	}
	//bfs(0);
	/*for(i=0; i<n; i++) 
		printf("%d ",dist[i]);	
	printf("\n\n");	*/
	memset(visited,0,sizeof(visited));
	dfs(0);

	for(i=0; i<n; i++) 
		printf("%d ",dtime[i]);	
	printf("\n");	
	for(i=0; i<n; i++) 
		printf("%d ",ftime[i]);	
	printf("\n");	
	return 0;
}
