#include <stdio.h>
#include <stdlib.h>
#define inf 100004
#define modp 1000000007

int dp[inf];
int s[8] = {1,2,5,10,20,50,100,200};


int main() {
	int n,m,i,j,t;
	m = 8;
	dp[0] = 1;
	for(i=1; i<inf; i++) dp[i] = 0;
	for(j=0; j<8; j++) for(i=s[j]; i<=inf; i++) dp[i] = (dp[i] + dp[i-s[j]])%modp; 
	scanf("%d",&t); 
	while(t--) {
		scanf("%d",&n);
		printf("%d\n",dp[n]);
	}
	return 0;
}
