													/*Online algo for longest non-repeated substring problem..*/
						/* Consider an array ind[d] of size of alphabet(d here is 26 for lower case letters). Initially set it to -1, and variables
						lo keeps the track of the start location of the start index of the current non repeated substring(need not to be max), 
						for every new character encountered we check it's index in ind, if the current index is i, and ind[] for that character is
						greater or equal to lo, means the character has already occurred in that non repeated substring, so this c is it's second 
						occurrance. 
						Then calculate the lenght of that non-repeated block i.e i-lo.. if it is greater than count then set it to count.
						And move lo to the new location, i.e make it ind[] + 1, since at index ind[] we have a character which is same as character at i.
						So new block starts from ind[]+1 (i.e lo) and this continues until i get equal to n.
						Again at last we check for count..*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max(x,y) (x)<(y)?(y):(x)
//d is alphabet size..
#define d	26

int main(){

	int ind[d]; //to keep indices..
	char c;		//stores the input character.
	int n,i,j,lo,count,m, ct;
	memset(ind,-1,d*sizeof(int));
	scanf("%d",&n);
	getchar();
	count = i = lo = 0;
	while(i<n) {
		c = getchar();
		//repeated if found..
		if(ind[c-97] >= lo) {
			ct = i - lo;
			count = ct > count?ct:count;
			lo = ind[c-97]+1;
		} 
		ind[c-97] = i;
		i++;
	}
	count = n-lo > count?i-lo:count;
	printf("%d\n",count);
	return 0;
}
