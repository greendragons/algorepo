#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max(x,y) (y)>(x)?(y):(x)

int main() {
	int i,j,k,n,W;
	int *dp,**ip;
	scanf("%d",&W);
	scanf("%d",&n);
	ip = (int **) malloc((n+1)*sizeof(int *));

	dp = (int *) malloc((W+1)*sizeof(int));
	memset(dp,0,sizeof(int)*(W+1));
	dp[0] = 0;

	//ip[0] = (int *)calloc(sizeof(int),2);
	for(i=1; i<=n; i++) {
		//for value and weight of i'th item..
		ip[i] = (int *)calloc(2,sizeof(int));
		scanf("%d%d",&ip[i][1],&ip[i][0]);
	}

	//solution..
	for(i=1; i<=n; i++) 
		for(j=W; j-ip[i][1] > 0; j--)
			dp[j] = max(dp[j], dp[j-ip[i][1]] + ip[i][0]); 
	printf("%d\n",dp[W]);

}
