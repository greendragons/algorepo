													/*Memoization version of knapsack problem.*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define max(x,y)  (x) <= (y) ? (y):(x)

int **dp, **ip;

int findsoln(int i, int j) {
	if (dp[i][j] == -1)  {
		if(j - ip[i][1] >= 0) {
			dp[i][j] = max(findsoln(i-1,j), findsoln(i-1, j-ip[i][1]) + ip[i][0]);
		} 
		else {
			dp[i][j] = findsoln(i-1,j);
		}
	}
	return dp[i][j];
}

int main() {
	int i,j,t,n,W;
	scanf("%d",&W);		//number of items in set..
	scanf("%d",&n);
	dp = (int **) calloc(n+1,sizeof(int *));
	ip = (int **) calloc(n+1,sizeof(int *));

	for(i=1; i<=n; i++)  {
		dp[i] = (int *)calloc(W+1,sizeof(int));
		for(j=1; j<=W; j++) dp[i][j] = -1;
		dp[i][0] = 0;
		ip[i] = (int *) calloc(2,sizeof(int *));
		//ip0 is value and ip1 is weight
		scanf("%d%d",&ip[i][1],&ip[i][0]);
	}

	dp[0] = (int *)calloc(W+1,sizeof(int));
	ip[0] = (int *)calloc(2,sizeof(int));

	for(i=0; i<n+1; i++) printf("%d %d\n",ip[i][0],ip[i][1]);
	printf("\n");
	for(i=0; i<n+1; i++) {for(j=0; j<=W; j++) {printf("%d ",dp[i][j]);} printf("\n");}

	//we want to find soln in dp[n][W]
	findsoln(n,W);

	for(i=0; i<n+1; i++) {for(j=0; j<=W; j++) {printf("%d ",dp[i][j]);} printf("\n");}

	//find opt soln itself.. not just value..
	printf("%d",dp[n][W]);
	printf("\n");
	i=n;
	j = W;
	while(j > 0 && i > 0) {
		if(j-ip[i][1] >=0 && dp[i-1][j] <= dp[i-1][j-ip[i][1]] + ip[i][0]) {
			printf("%d ",ip[i][0]);
			j -= ip[i][1];
		}
		else j--;
		i--;
	}
	printf("\n");
}
