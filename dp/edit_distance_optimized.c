#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define MAX(x,y) x<y?y:x
#define MIN(x,y) x>y?y:x
#define ull unsigned long long
#define gc getchar_unlocked
#define FOR(i,n) for(i=0; i<n; i++)
#define FORE(i,x,n) for(i=x; i<=n; i++)
#define rep(n) while(n--) 
#define NL printf("\n")
#define CHAR_BIT 8
#define min(x,y) (y) ^ (((x) ^ (y)) & -((x) < (y))) 
#define inf 2009

inline int ABS(int x) {
	int mask;
	mask = x >> sizeof(x) * CHAR_BIT - 1;
	x = (x + mask) ^ mask;
	return x;
}

inline int read_int(void)
{
	char t;
	int x=0;
	while((t=gc())&&(t<48 || t>57));
	while(t>=48 && t<=57) {
		x=(x<<3)+(x<<1)+t-48;
		t=gc();
	}
	return x;
}

char a[inf], b[inf];
int dp[inf];

inline int solve(int l1, int l2) {
	//v1 and v2 for two values dp[i-1][j] and dp[i-1][j-1]
	int i,j,v1,v2;
	FORE(i,1,l1) {
		v2 = dp[0];
		dp[0] = i;
		v1 = dp[1];
		FORE(j,1,l2) {
			if(a[i-1]^b[j-1]) dp[j] = min(v2 +1, min(v1+1, dp[j-1]+1));
			else dp[j] = min(v2, min(v1+1, dp[j-1]+1));
			v2 = v1;
			v1 = dp[j+1];
		}
	}
	return dp[l2];
}

int main() {
	int t,i;
	t = read_int();
	rep(t) {
		scanf("%s%s",a,b);		
		//fill all entries of form as dp[0][j] = j;
		FOR(i,strlen(b)+1) dp[i] = i; 
		printf("%d\n",solve(strlen(a),strlen(b)));
	}
	return 0;
}
