													/*This version returns 1 if subset exists whose sum is equal to variable sum
													else 0.. this program uses optimized approach and only uses one table..*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define inf 1002

char table[inf];

void printtable(int sum) {
	int i;	
	for(i=0;i<=sum; i++) printf("%d ",table[i]);
	printf("\n");
}

int main() {
	int i,j,k,m,sum,s[inf];
	scanf("%d",&m);
	memset(table,0,inf);
	table[0] = 1;
	for(i=0; i<m; i++) scanf("%d",&s[i]);
	scanf("%d",&sum);
	for(i=0; i<m; i++) for(j=sum; j>0; j--) table[j] |= table[j-s[i]];	
	printf("%d\n",table[sum]);
}
