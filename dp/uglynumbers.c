							/*Ugly numbers are numbers whose factors are only 2,3 and 5, e.g. 1,2,3,4,5,6,8,10,12,15,18,20...
																Problem Source: G4G*/
#include <stdio.h>
#include <stdlib.h>
int main() {
	int n,i,j,k,c;
	unsigned long long temp1,temp2, dp[1000] = {0,1,2,3,4,5},max,v1,v2,v3;
	//ask user to enter which ugly number is required...
	scanf("%d",&n);
	c = 5; i = 3; j = 3; k = 5;
	while(c <= n) {
		v1 = 2*dp[i];	
		v2 = 3*dp[j];	
		v3 = 5*dp[k];	
		max = v1<v2?v1:v2;
		max = max<v3?max:v3;
		dp[++c] = max;
		if(max == v1) i++;
		if(max == v2) j++;
		if(max == v3) k++;
	}
	//for(i=1; i<=n; i++) printf("%llu ",dp[i]);
	printf("%llu\n",dp[n]);
}
