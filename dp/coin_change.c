					/* Given a value N, if we want to make change for N cents, and we have infinite supply of each of 
						S = { S1, S2, .. , Sm} valued coins, how many ways can we make the change? The order of coins doesn’t matter.  

						For example, for N = 4 and S = {1,2,3}, there are four solutions: {1,1,1,1},{1,1,2},{2,2},{1,3}. So output should be 4.
						For N = 10 and S = {2, 5, 3, 6}, there are five solutions: {2,2,2,2,2}, {2,2,3,3}, {2,2,6}, {2,3,5} and {5,5}. 
						So the output should be 5.

					*/
#include <stdio.h>
#include <stdlib.h>
#define inf 100

int dp[inf][inf];
int s[inf];

unsigned long long soln(int i, int n) {
	unsigned long long count = 0;
	int k;
	if(dp[n][i] != -1) return dp[n][i];
	else if(!i) {
		if(n%s[0] == 0)	dp[n][i] = 1; 
		else dp[n][i] = 0;
		return dp[n][i];
	}

	for(k=0; n-k*s[i]>=0; k++) {
		count += soln(i-1,n-k*s[i]);	
	}
	dp[n][i] = count;
    return dp[n][i];
}

int main() {
	int n,m,i,j;
	scanf("%d",&m);
	for(i=0; i<m; i++) scanf("%d",&s[i]);
	scanf("%d",&n);
	for(i=0; i<=n; i++) for(j=0; j<m; j++) dp[i][j] = -1; 
	printf("%llu\n",soln(m-1,n));
	return 0;
}
