#include <stdio.h>
#include <string.h>
#define inf 100
#define MAX(x,y)  (x<y)?y:x
int main() {
	int i,j,k;
	int opt[inf][inf];
	char opstr[inf];
	int n;
	char str[inf];
	scanf("%s",str);
	n = strlen(str);
	//set 0's in the soln and at locs (i,i) set 1's 
	for(i=0; i<n; i++) 	
		for(j=0; j<n; j++) 	
			if(i == j) opt[i][j] = 1;
			else opt[i][j] = 0; 
	//dp table filling...
	for(i=n; i>0; i--) {
		for(j=i+1; j<=n; j++)	{
				if(str[i-1] == str[j-1])
					opt[i][j] = opt[i+1][j-1] + 2;
				else
					opt[i][j] = MAX(opt[i+1][j],opt[i][j-1]);
		}
	}
	printf("%d\n",opt[1][n]);

	//bug in finding the actual palindrome..
	i=1,j=n,k=0;
	while(i<=n && j>=0) {
			if(str[i-1] == str[j-1]) {
				opstr[k++] = str[i-1];
				i++,j--;
			}
			else if(opt[i+1][j] >= opt[i][j-1])
				i++;	
			else j--;
	}
	for(i=--k; i>=0; i--)
		printf("%c",opstr[i]);
	printf("\n");
	
}
