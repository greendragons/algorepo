#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define inf 1000

char primes[inf];
void sieve(int n) {
	int i,j;	
	primes[0] = primes[1] = 1;
	while(i<inf) {if(!primes[i]) 
		for(j=i*i; j<inf; j+=i) primes[j] = 1; 
		i++;
	} 
}

int main() {
	int i;
	sieve(200);
	for(i=0; i<=100; i++) if(!primes[i]) printf("%d ",i);
	printf("\n");
}
