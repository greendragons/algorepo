#include<bits/stdc++.h>
#define LIM 1000

using namespace std;

int p[LIM];

void compute_prefix(string &s) {
	int i,j=0;	
	//as we want proper prefixes..
	p[0] = 0;
	j = 0;
	for(i=1; i<s.size(); i++) {
		while(j>0 && s[j] != s[i])
			j = p[j-1];
		if(s[j] == s[i])
			j++;
		p[i] = j;
	}
}

inline void matcher(char *t, char *p, int n, int m) {
	 int i,k,*pfn;
	 pfn = prefix_compute(p,m);
	 k=0,i=0;
	 while(i<n) {
		 while(t[i] != p[k] && k>=0) k = pfn[k];
		 k++;i++;
		 if(k == m) {
		 		printf("match found at %d\n",i-k);
		 		k = pfn[k];
		 }
	 }
}

void matcher() {

}

int main() {
	int i,j;
	string s;
	cin>>s;
	compute_prefix(s);
	for(i=0; i<s.size(); i++)
		cout<<p[i]<<" ";
	cout<<endl;
}
