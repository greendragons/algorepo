#include <bits/stdc++.h>
#define _ std::ios_base::Init i; std::ios_base::sync_with_stdio(false); std::cin.tie(0);
#define pb push_back
#define INF 99999999

using namespace std;

typedef vector<int> vi;
typedef vector<vi> vii;
typedef long long ll;
typedef unsigned long long ull;

int find_max_sum(vi& a) {
	int msum = -INF,i, sum=0;

	for(i=0; i<a.size(); i++) {
		sum += a[i];			
		if(sum > msum) {
			msum = sum;	
		}
		if(sum < 0) {
			sum = a[i];	
		}
	}
	return msum;
} 

int find_max_dp(vi& a) {
   int max_so_far = a[0], i;
   int curr_max = a[0];
 
   for (i = 1; i < a.size(); i++)
   {
        curr_max = max(a[i], curr_max+a[i]);
        max_so_far = max(max_so_far, curr_max);
   }
   return max_so_far;
}

int main() {
	vi arr(1000);
	int i,j,n;
	cin>>n;
	for(i=0; i<n; i++) {
		cin>>arr[i];	
	}

	cout<<find_max_dp(arr)<<endl;
	return 0;
}
